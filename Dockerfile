ARG BASE_IMAGE_TAG
From wodby/python:$BASE_IMAGE_TAG

USER root
# Install additional deps:
# - gettext: needed for envsubs binary used in init scripts.
# - ttf-freefont, msttcorefonts-installer: fonts required for pdf generation.
# - wait4ports: for init scripts.
RUN set -ex; \
    \
    apk add --no-cache build-base libffi-dev openssl-dev

USER wodby

COPY requirements-gcc.txt ./
RUN pip install --no-cache-dir -r requirements-gcc.txt
